import React from "react";
import { Route, Switch } from "react-router-dom";
import ScrollToTop from "react-router-scroll-top";
import "./css/tailwind.css";
import Heading from "./components/Heading";
import Home from "./components/Home";
import Footer from "./components/Footer";
import CenteredColumn from "./components/CenteredColumn";
import CenteredColumns from "./components/CenteredColumns";
import CenteredColumnsWithGutters from "./components/CenteredColumnsWithGutters";
import LeftJustifiedColumns from "./components/LeftJustifiedColumns";
import RightJustifiedColumns from "./components/RightJustifiedColumns";
import AlignedTextInput from "./components/AlignedTextInput";
import CenteredButton from "./components/CenteredButton";
import CenteredButtonGroup from "./components/CenteredButtonGroup";
import PreviousNextButtons from "./components/PreviousNextButtons";
import SimpleHeading from "./components/SimpleHeading";
import SimpleFooter from "./components/SimpleFooter";
import CenteredHorizontalList from "./components/CenteredHorizontalList";
import Card from "./components/Card";
import MediaObject from "./components/MediaObject";
import Score from "./components/Score";
import TableRow from "./components/TableRow";
import TableFilter from "./components/TableFilter";
import FlexWrap from "./components/FlexWrap";
import NavWithIcon from "./components/NavWithIcon";

function App() {
  return (
    <div className="">
      <Heading />
      <main>
        <Switch>
          <ScrollToTop>
            <Route path="/" component={Home} exact />
            <Route path="/centered-column" component={CenteredColumn} />
            <Route path="/centered-columns" component={CenteredColumns} />
            <Route
              path="/centered-columns-with-gutters"
              component={CenteredColumnsWithGutters}
            />
            <Route
              path="/left-justified-columns"
              component={LeftJustifiedColumns}
            />
            <Route
              path="/right-justified-columns"
              component={RightJustifiedColumns}
            />
            <Route path="/aligned-text-input" component={AlignedTextInput} />
            <Route path="/centered-button" component={CenteredButton} />
            <Route
              path="/centered-button-group"
              component={CenteredButtonGroup}
            />
            <Route
              path="/previous-next-buttons"
              component={PreviousNextButtons}
            />
            <Route path="/simple-heading" component={SimpleHeading} />
            <Route path="/simple-footer" component={SimpleFooter} />
            <Route
              path="/centered-horizontal-list"
              component={CenteredHorizontalList}
            />
            <Route path="/card" component={Card} />
            <Route path="/media-object" component={MediaObject} />
            <Route path="/score" component={Score} />
            <Route path="/table-row" component={TableRow} />
            <Route path="/table-filter" component={TableFilter} />
            <Route path="/flex-wrap" component={FlexWrap} />
            <Route path="/nav-with-icon" component={NavWithIcon} />
          </ScrollToTop>
        </Switch>
      </main>
      <Footer />
    </div>
  );
}

export default App;
