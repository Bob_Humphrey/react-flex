import React from "react";
import Prism from "prismjs";
require("../css/prism.css");

const code = `
<div className="flex w-full justify-center pt-3 pb-4 text-warm-gray-700 bg-warm-gray-200">
    <div className="text-xl text-center leading-tight">Heading</div>
</div>
`.trim();

class SimpleHeading extends React.Component {
  componentDidMount() {
    // You can call the Prism.js API here
    // Use setTimeout to push onto callback queue so it runs after the DOM is updated
    setTimeout(() => Prism.highlightAll(), 0);
  }
  render() {
    return (
      <div className="flex w-full justify-center">
        <div className="w-5/6">
          <h2 className="w-full text-2xl text-center pt-4 pb-6">
            Simple Heading
          </h2>
          <div className="flex w-full justify-center pt-3 pb-4 text-warm-gray-700 bg-warm-gray-200">
            <div className="text-xl text-center leading-tight">Heading</div>
          </div>
          <div className="py-3"></div>
          <pre className="w-full">
            <code className="language-html">{code}</code>
          </pre>
        </div>
      </div>
    );
  }
}

export default SimpleHeading;
