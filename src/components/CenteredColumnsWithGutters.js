import React from "react";
import Prism from "prismjs";
require("../css/prism.css");

const code = `
<div className="flex w-full justify-center bg-warm-gray-200 py-6">
    <div className="lg:flex w-2/3 lg:-mx-2">
        <div className="w-full lg:w-1/3 lg:mx-2 mb-2 lg:mb-0 bg-warm-gray-400 h-64"></div>
        <div className="w-full lg:w-1/3 lg:mx-2 mb-2 lg:mb-0 bg-warm-gray-300 h-64"></div>
        <div className="w-full lg:w-1/3 lg:mx-2 mb-2 lg:mb-0 bg-warm-gray-400 h-64"></div>
    </div>
</div>
`.trim();

class CenteredColumnsWithGutters extends React.Component {
  componentDidMount() {
    // You can call the Prism.js API here
    // Use setTimeout to push onto callback queue so it runs after the DOM is updated
    setTimeout(() => Prism.highlightAll(), 0);
  }
  render() {
    return (
      <div className="flex w-full justify-center">
        <div className="w-5/6">
          <h2 className="w-full text-2xl leading-tight text-center pt-4 pb-6">
            Centered Columns With Gutters
          </h2>
          <div className="flex w-full justify-center bg-warm-gray-200 py-6">
            <div className="lg:flex w-2/3 lg:-mx-2">
              <div className="w-full lg:w-1/3 lg:mx-2 mb-2 lg:mb-0 bg-warm-gray-400 h-64"></div>
              <div className="w-full lg:w-1/3 lg:mx-2 mb-2 lg:mb-0 bg-warm-gray-300 h-64"></div>
              <div className="w-full lg:w-1/3 lg:mx-2 mb-2 lg:mb-0 bg-warm-gray-400 h-64"></div>
            </div>
          </div>
          <div className="py-3"></div>
          <pre className="w-full">
            <code className="language-html">{code}</code>
          </pre>
        </div>
      </div>
    );
  }
}

export default CenteredColumnsWithGutters;
