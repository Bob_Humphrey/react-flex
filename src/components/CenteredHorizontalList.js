import React from "react";
import Prism from "prismjs";
require("../css/prism.css");

const code = `
<div className="flex justify-center flex-col lg:flex-row w-full bg-warm-gray-200 text-warm-gray-700 py-10 mb-6">
    <a className="px-4" href="/">
        Experience
    </a>
    <a className="px-4" href="/">
        Projects
    </a>
    <a className="px-4" href="/">
        Skills
    </a>
</div>
`.trim();

class CenteredHorizontalList extends React.Component {
  componentDidMount() {
    // You can call the Prism.js API here
    // Use setTimeout to push onto callback queue so it runs after the DOM is updated
    setTimeout(() => Prism.highlightAll(), 0);
  }
  render() {
    return (
      <div className="flex w-full justify-center">
        <div className="w-5/6">
          <h2 className="w-full text-2xl text-center pt-4 pb-6">
            Centered Horizontal List
          </h2>
          <div className="flex justify-center flex-col lg:flex-row w-full bg-warm-gray-200 text-warm-gray-700 py-10 mb-6">
            <a className="px-4" href="/">
              Experience
            </a>
            <a className="px-4" href="/">
              Projects
            </a>
            <a className="px-4" href="/">
              Skills
            </a>
          </div>
          <div className="py-3"></div>
          <pre className="w-full">
            <code className="language-html">{code}</code>
          </pre>
        </div>
      </div>
    );
  }
}

export default CenteredHorizontalList;
