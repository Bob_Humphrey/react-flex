import React from "react";
import Prism from "prismjs";
require("../css/prism.css");

const code = `
<div className=" flex w-full lg:w-1/3 justify-center bg-warm-gray-200 py-6">
    <div className="bg-warm-gray-400 h-4 w-4 mx-3 rounded-lg">
    &nbsp;
    </div>
    <div className="bg-warm-gray-400 h-4 w-4 mx-3 rounded-lg">
    &nbsp;
    </div>
    <div className="bg-warm-gray-800 h-4 w-4 mx-3 rounded-lg">
    &nbsp;
    </div>
    <div className="bg-warm-gray-400 h-4 w-4 mx-3 rounded-lg">
    &nbsp;
    </div>
    <div className="bg-warm-gray-400 h-4 w-4 mx-3 rounded-lg">
    &nbsp;
    </div>
</div>
`.trim();

class Score extends React.Component {
  componentDidMount() {
    // You can call the Prism.js API here
    // Use setTimeout to push onto callback queue so it runs after the DOM is updated
    setTimeout(() => Prism.highlightAll(), 0);
  }
  render() {
    return (
      <div className="flex w-full justify-center">
        <div className="w-5/6">
          <h2 className="w-full text-2xl text-center pt-4 pb-6">Score</h2>
          <div className="flex w-full justify-center">
            <div className=" flex w-full lg:w-1/3 justify-center bg-warm-gray-200 py-6">
              <div className="bg-warm-gray-400 h-4 w-4 mx-3 rounded-lg">
                &nbsp;
              </div>
              <div className="bg-warm-gray-400 h-4 w-4 mx-3 rounded-lg">
                &nbsp;
              </div>
              <div className="bg-warm-gray-800 h-4 w-4 mx-3 rounded-lg">
                &nbsp;
              </div>
              <div className="bg-warm-gray-400 h-4 w-4 mx-3 rounded-lg">
                &nbsp;
              </div>
              <div className="bg-warm-gray-400 h-4 w-4 mx-3 rounded-lg">
                &nbsp;
              </div>
            </div>
          </div>
          <div className="py-3"></div>
          <pre className="w-full">
            <code className="language-html">{code}</code>
          </pre>
        </div>
      </div>
    );
  }
}

export default Score;
