import React from "react";
import logo from "./../images/bh-logo-grey.gif";

const Heading = () => {
  return (
    <footer className="flex w-full justify-center py-16">
      <div className="w-16">
        <a href="https://bob-humphrey.com">
          <img src={logo} alt="Logo" />
        </a>
      </div>
    </footer>
  );
};

export default Heading;
