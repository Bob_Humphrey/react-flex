import React from "react";
import Prism from "prismjs";
require("../css/prism.css");

const code = `
<div className="flex flex-col lg:flex-row lg:w-2/3 items-start bg-warm-gray-200 p-10 mb-6">
    <div className="w-32 h-32 mr-8 mb-4 lg:mb-0 bg-warm-gray-400"></div>
    <div className="flex-1">
        <h3 className="text-3xl font-bold text-warm-gray-700 mb-4 lg:-mt-3">
            Title
        </h3>
        <p className="text-warm-gray-700 text-justify">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
            pharetra lacus quis enim feugiat pharetra. Nunc lobortis
            sodales nisi, et consectetur purus iaculis vel. Integer
            fermentum turpis vel erat euismod, et venenatis felis finibus.
            Cras auctor sapien tellus, sit amet tincidunt elit luctus et.
            Aliquam erat volutpat. Ut ut enim id nisi sodales feugiat. Sed
            vitae placerat magna. Interdum et malesuada fames ac ante
            ipsum primis in faucibus. Curabitur eu imperdiet urna. Integer
            gravida gravida turpis ac lobortis. Aliquam dui sapien, mollis
            fringilla condimentum sit amet, pellentesque ac neque.
        </p>
    </div>
</div>
`.trim();

class MediaObject extends React.Component {
  componentDidMount() {
    // You can call the Prism.js API here
    // Use setTimeout to push onto callback queue so it runs after the DOM is updated
    setTimeout(() => Prism.highlightAll(), 0);
  }
  render() {
    return (
      <div className="flex w-full justify-center">
        <div className="w-5/6">
          <h2 className="w-full text-2xl text-center pt-4 pb-6">Media Object</h2>
          <div className="lg:flex justify-center">
            <div className="flex flex-col lg:flex-row lg:w-2/3 items-start bg-warm-gray-200 p-10 mb-6">
              <div className="w-32 h-32 mr-8 mb-4 lg:mb-0 bg-warm-gray-400"></div>
              <div className="flex-1">
                <h3 className="text-3xl font-bold text-warm-gray-700 mb-4 lg:-mt-3">
                  Title
                </h3>
                <p className="text-warm-gray-700 text-justify">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                  pharetra lacus quis enim feugiat pharetra. Nunc lobortis
                  sodales nisi, et consectetur purus iaculis vel. Integer
                  fermentum turpis vel erat euismod, et venenatis felis finibus.
                  Cras auctor sapien tellus, sit amet tincidunt elit luctus et.
                  Aliquam erat volutpat. Ut ut enim id nisi sodales feugiat. Sed
                  vitae placerat magna. Interdum et malesuada fames ac ante
                  ipsum primis in faucibus. Curabitur eu imperdiet urna. Integer
                  gravida gravida turpis ac lobortis. Aliquam dui sapien, mollis
                  fringilla condimentum sit amet, pellentesque ac neque.
                </p>
              </div>
            </div>
          </div>
          <div className="py-3"></div>
          <pre className="w-full">
            <code className="language-html">{code}</code>
          </pre>
        </div>
      </div>
    );
  }
}

export default MediaObject;
