import React from "react";
import Prism from "prismjs";
require("../css/prism.css");

const code = `
<div className="flex w-full justify-center bg-warm-gray-200 pt-6 pb-4">
    <div className="bg-warm-gray-400 text-sm text-center rounded-full cursor-pointer px-2 mx-2 mb-2">
        All
    </div>
    <div className="bg-warm-gray-700 text-white text-sm text-center rounded-full cursor-pointer px-2 mx-2 mb-2">
        Open
    </div>
    <div className="bg-warm-gray-400 text-sm text-center rounded-full cursor-pointer px-2 mx-2 mb-2">
        Closed
    </div>
</div>
`.trim();

class TableFilter extends React.Component {
  componentDidMount() {
    // You can call the Prism.js API here
    // Use setTimeout to push onto callback queue so it runs after the DOM is updated
    setTimeout(() => Prism.highlightAll(), 0);
  }
  render() {
    return (
      <div className="flex w-full justify-center">
        <div className="w-5/6">
          <h2 className="w-full text-2xl text-center pt-4 pb-6">Table Filter</h2>
          <div className="flex w-full justify-center bg-warm-gray-200 pt-6 pb-4">
            <div className="bg-warm-gray-400 text-sm text-center rounded-full cursor-pointer px-2 mx-2 mb-2">
              All
            </div>
            <div className="bg-warm-gray-700 text-white text-sm text-center rounded-full cursor-pointer px-2 mx-2 mb-2">
              Open
            </div>
            <div className="bg-warm-gray-400 text-sm text-center rounded-full cursor-pointer px-2 mx-2 mb-2">
              Closed
            </div>
          </div>
          <div className="py-3"></div>
          <pre className="w-full">
            <code className="language-html">{code}</code>
          </pre>
        </div>
      </div>
    );
  }
}

export default TableFilter;
