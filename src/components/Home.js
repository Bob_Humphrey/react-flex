import React from "react";
import { NavLink } from "react-router-dom";
import logo from "./../images/bh-logo-grey.gif";

const Home = () => {
  const openPath =
    "M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zM9 11v4h2V9H9v2zm0-6v2h2V5H9z";
  return (
    <div className="flex w-full justify-center">
      <div className="lg:flex w-5/6 py-6 -mx-2">
        {/* COLUMN 1 */}

        <div className="w-full lg:w-1/3 px-2">
          <NavLink to="/centered-column">
            <div className="flex w-full justify-center bg-warm-gray-200 py-6">
              <div className="w-2/3 bg-warm-gray-400 h-24"></div>
            </div>
            <div className="text-center py-2">Centered Column</div>
          </NavLink>

          <NavLink to="/centered-columns">
            <div className="flex w-full justify-center bg-warm-gray-200 py-6">
              <div className="lg:flex w-2/3">
                <div className="w-full lg:w-1/3 bg-warm-gray-400 h-24"></div>
                <div className="w-full lg:w-1/3 bg-warm-gray-300 h-24"></div>
                <div className="w-full lg:w-1/3 bg-warm-gray-400 h-24"></div>
              </div>
            </div>
            <div className="text-center py-2">Centered Columns</div>
          </NavLink>

          <NavLink to="/centered-columns-with-gutters">
            <div className="flex w-full justify-center bg-warm-gray-200 py-6">
              <div className="lg:flex w-2/3 lg:-mx-2">
                <div className="w-full lg:w-1/3 lg:mx-2 mb-2 lg:mb-0 bg-warm-gray-400 h-24"></div>
                <div className="w-full lg:w-1/3 lg:mx-2 mb-2 lg:mb-0 bg-warm-gray-300 h-24"></div>
                <div className="w-full lg:w-1/3 lg:mx-2 mb-2 lg:mb-0 bg-warm-gray-400 h-24"></div>
              </div>
            </div>
            <div className="text-center py-2">
              Centered Columns With Gutters
            </div>
          </NavLink>

          <NavLink to="/left-justified-columns">
            <div className="lg:flex justify-start bg-warm-gray-200 py-6 px-6">
              <div className="w-full lg:w-1/3 bg-warm-gray-300 h-24 mb-2 lg:mb-0"></div>
              <div className="w-full lg:w-1/3 bg-warm-gray-400 h-24"></div>
            </div>
            <div className="text-center py-2">Left Justified Columns</div>
          </NavLink>

          <NavLink to="/right-justified-columns">
            <div className="lg:flex justify-end bg-warm-gray-200 py-6 px-6">
              <div className="w-full lg:w-1/3 bg-warm-gray-300 h-24 mb-2 lg:mb-0"></div>
              <div className="w-full lg:w-1/3 bg-warm-gray-400 h-24"></div>
            </div>
            <div className="text-center py-2">Right Justified Columns</div>
          </NavLink>

          <NavLink to="/flex-wrap">
            <div className="w-full bg-warm-gray-200 p-8">
              <div className="flex flex-wrap -mx-1">
                <div className="w-1/2 lg:w-1/4 px-1 mb-2">
                  <div className="bg-warm-gray-400 h-24"></div>
                </div>
                <div className="w-1/2 lg:w-1/4 px-1 mb-2">
                  <div className="bg-warm-gray-400 h-24"></div>
                </div>
                <div className="w-1/2 lg:w-1/4 px-1 mb-2">
                  <div className="bg-warm-gray-400 h-24"></div>
                </div>
                <div className="w-1/2 lg:w-1/4 px-1 mb-2">
                  <div className="bg-warm-gray-400 h-24"></div>
                </div>
                <div className="w-1/2 lg:w-1/4 px-1 mb-2">
                  <div className="bg-warm-gray-400 h-24"></div>
                </div>
                <div className="w-1/2 lg:w-1/4 px-1 mb-2">
                  <div className="bg-warm-gray-400 h-24"></div>
                </div>
                <div className="w-1/2 lg:w-1/4 px-1 mb-2">
                  <div className="bg-warm-gray-400 h-24"></div>
                </div>
                <div className="w-1/2 lg:w-1/4 px-1 mb-2">
                  <div className="bg-warm-gray-400 h-24"></div>
                </div>
              </div>
            </div>
            <div className="text-center py-2">Flex Wrap</div>
          </NavLink>
        </div>

        {/* COLUMN 2 */}

        <div className="w-full lg:w-1/3 px-2">
          <NavLink to="/aligned-text-input">
            <div className="flex justify-center w-full bg-warm-gray-200 py-6">
              <form
                className="bg-warm-gray-300 w-4/5 lg:w-5/6 py-6 px-8 lg:px-8"
                action=""
              >
                <div className="lg:flex mb-4 -mx-2">
                  <div className="lg:w-1/3">
                    <label
                      className="block py-2 text-warm-gray-500"
                      for="first_name"
                    >
                      First Name
                    </label>
                  </div>
                  <div className="lg:w-2/3 bg-white py-2 px-4 rounded">
                    <input
                      className="w-full bg-white"
                      id="first_name"
                      name="first_name"
                      type="text"
                      value=""
                    />
                  </div>
                </div>
                <div className="lg:flex -mx-2">
                  <div className="lg:w-1/3">
                    <label
                      className="block py-2 text-warm-gray-500"
                      for="first_name"
                    >
                      Last Name
                    </label>
                  </div>
                  <div className="lg:w-2/3 bg-white py-2 px-4 rounded">
                    <input
                      className="w-full bg-white"
                      id="last_name"
                      name="last_name"
                      type="text"
                      value=""
                    />
                  </div>
                </div>
              </form>
            </div>
            <div className="text-center py-2">Aligned Text Input</div>
          </NavLink>

          <NavLink to="/centered-button">
            <div className="flex justify-center w-full bg-warm-gray-200 py-10">
              <a
                className="bg-warm-gray-400 hover:bg-warm-gray-800 text-warm-gray-700 hover:text-white text-center rounded py-2 w-48 "
                href="/"
              >
                Send
              </a>
            </div>
            <div className="text-center py-2">Centered Button</div>
          </NavLink>

          <NavLink to="/centered-button-group">
            <div className="flex flex-col lg:flex-row items-center lg:justify-center w-full bg-warm-gray-200 py-10">
              <a
                className="bg-warm-gray-400 hover:bg-warm-gray-800 text-warm-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-32 "
                href="/"
              >
                Send
              </a>
              <a
                className="bg-warm-gray-400 hover:bg-warm-gray-800 text-warm-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-32 "
                href="/"
              >
                Cancel
              </a>
            </div>
            <div className="text-center py-2">Centered Button Group</div>
          </NavLink>

          <NavLink to="/previous-next-buttons">
            <div className="flex flex-col lg:flex-row items-center lg:justify-between w-full bg-warm-gray-200 py-10 lg:px-4">
              <a
                className="lg:justify-start bg-warm-gray-400 hover:bg-warm-gray-800 text-warm-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-32 "
                href="/"
              >
                Previous
              </a>
              <a
                className="lg:justify-end bg-warm-gray-400 hover:bg-warm-gray-800 text-warm-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-32 "
                href="/"
              >
                Next
              </a>
            </div>
            <div className="text-center py-2">Previous Next Buttons</div>
          </NavLink>

          <NavLink to="/score">
            <div className=" flex w-full justify-center bg-warm-gray-200 py-6">
              <div className="bg-warm-gray-400 h-4 w-4 mx-3 rounded-lg">
                &nbsp;
              </div>
              <div className="bg-warm-gray-400 h-4 w-4 mx-3 rounded-lg">
                &nbsp;
              </div>
              <div className="bg-warm-gray-800 h-4 w-4 mx-3 rounded-lg">
                &nbsp;
              </div>
              <div className="bg-warm-gray-400 h-4 w-4 mx-3 rounded-lg">
                &nbsp;
              </div>
              <div className="bg-warm-gray-400 h-4 w-4 mx-3 rounded-lg">
                &nbsp;
              </div>
            </div>
            <div className="text-center py-2">Score</div>
          </NavLink>

          <NavLink to="/table-row">
            <div className=" flex flex-col lg:flex-row w-full justify-center bg-warm-gray-200 text-warm-gray-700 border-t border-l border-r border-warm-gray-200 pt-4 pb-4 px-6">
              <div className="w-1/3">Data</div>
              <div className="w-1/3">Data</div>
              <div className="w-1/3">Data</div>
            </div>
            <div className="text-center py-2">Table Row</div>
          </NavLink>

          <NavLink to="/table-filter">
            <div className="flex w-full justify-center bg-warm-gray-200 pt-6 pb-4">
              <div className="bg-warm-gray-400 text-sm text-center rounded-full cursor-pointer px-2 mx-2 mb-2">
                All
              </div>
              <div className="bg-warm-gray-700 text-white text-sm text-center rounded-full cursor-pointer px-2 mx-2 mb-2">
                Open
              </div>
              <div className="bg-warm-gray-400 text-sm text-center rounded-full cursor-pointer px-2 mx-2 mb-2">
                Closed
              </div>
            </div>
            <div className="text-center py-2">Table Filter</div>
          </NavLink>

          <NavLink to="/nav-with-icon">
            <div className="flex flex-col lg:flex-row w-full items-center lg:justify-between bg-warm-gray-200 py-4">
              <div className="flex justify-start text-warm-gray-700">
                <div className="text-2xl text-center leading-tight">
                  Heading
                </div>
              </div>
              <div className="flex justify-end self-center lg:self-end text-warm-gray-700 cursor-pointer mt-2">
                <svg
                  className="h-8 w-8 pb-1 fill-current"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 20 20"
                >
                  <path d={openPath} />
                </svg>
              </div>
            </div>
            <div className="text-center py-2">Nav With Icon</div>
          </NavLink>
        </div>

        {/* COLUMN 3 */}

        <div className="w-full lg:w-1/3 px-2">
          <NavLink to="/simple-heading">
            <div className="flex w-full justify-center pt-3 pb-4 text-warm-gray-700 bg-warm-gray-200">
              <div className="text-xl text-center leading-tight">Heading</div>
            </div>
            <div className="text-center py-2">Simple Heading</div>
          </NavLink>

          <NavLink to="/simple-footer">
            <div className="flex w-full justify-center bg-warm-gray-200 py-8">
              <div className="w-16">
                <a href="https://bob-humphrey.com">
                  <img src={logo} alt="Logo" />
                </a>
              </div>
            </div>
            <div className="text-center py-2">Simple Footer</div>
          </NavLink>

          <NavLink to="/centered-horizontal-list">
            <div className="flex justify-center flex-col lg:flex-row w-full bg-warm-gray-200 text-warm-gray-700 py-10">
              <a className="px-4" href="/">
                Experience
              </a>
              <a className="px-4" href="/">
                Projects
              </a>
              <a className="px-4" href="/">
                Skills
              </a>
            </div>
            <div className="text-center py-2">Centered Horizontal List</div>
          </NavLink>

          <NavLink to="/card">
            <div className="flex w-full items-stretch">
              <div className="bg-warm-gray-200 rounded-lg">
                <div className="rounded-t-lg bg-warm-gray-400 h-32"> </div>
                <div className="py-4 px-8">
                  <h3 className="text-warm-gray-700 text-2xl pb-2">Title</h3>
                  <div className="pb-6 text-warm-gray-700 text-justify">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Curabitur eget elit eu urna gravida semper quis eget quam.
                  </div>
                  <div className="flex justify-center">
                    <div className="bg-warm-gray-400 hover:bg-warm-gray-800 text-warm-gray-700 hover:text-white rounded px-8 py-1 mr-2 mb-2">
                      <a className="" href="/">
                        Visit Site
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="text-center py-2">Card</div>
          </NavLink>
          <NavLink to="/media-object">
            <div className="flex flex-col lg:flex-row items-start bg-warm-gray-200 p-10">
              <div className="w-24 h-24 mr-8 mb-4 lg:mb-0 bg-warm-gray-400"></div>
              <div className="flex-1">
                <h3 className="text-2xl text-warm-gray-700 mb-4 lg:-mt-3">
                  Title
                </h3>
                <p className="text-warm-gray-700  text-justify">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                  pharetra lacus quis enim feugiat pharetra. Nunc lobortis
                  sodales nisi, et consectetur purus iaculis vel. Integer
                  fermentum turpis vel erat euismod, et venenatis felis finibus.
                  Cras auctor sapien tellus, sit amet tincidunt elit luctus et.
                </p>
              </div>
            </div>
            <div className="text-center py-2">Media Object</div>
          </NavLink>
        </div>
      </div>
    </div>
  );
};

export default Home;
