import React from "react";
import Prism from "prismjs";
require("../css/prism.css");

const code = `
<div className="flex flex-col lg:flex-row w-full items-center lg:justify-between bg-warm-gray-200 py-4">
    <div className="flex justify-start text-warm-gray-700">
        <div className="text-3xl text-center leading-tight">Heading</div>
    </div>
    <div className="flex justify-end self-center lg:self-end text-warm-gray-700 cursor-pointer mt-2">
        <svg
        className="h-8 w-8 pb-1 fill-current"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 20 20"
        >
        <path d={openPath} />
        </svg>
    </div>
</div>
`.trim();
const openPath =
  "M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zM9 11v4h2V9H9v2zm0-6v2h2V5H9z";

class NavWithIcon extends React.Component {
  componentDidMount() {
    // You can call the Prism.js API here
    // Use setTimeout to push onto callback queue so it runs after the DOM is updated
    setTimeout(() => Prism.highlightAll(), 0);
  }

  render() {
    return (
      <div className="flex w-full justify-center">
        <div className="w-5/6">
          <h2 className="w-full text-2xl text-center pt-4 pb-6">
            Nav With Icon
          </h2>
          <div className="flex flex-col lg:flex-row w-full items-center lg:justify-between bg-warm-gray-200 py-4">
            <div className="flex justify-start text-warm-gray-700">
              <div className="text-3xl text-center leading-tight">Heading</div>
            </div>
            <div className="flex justify-end self-center lg:self-end text-warm-gray-700 cursor-pointer mt-2">
              <svg
                className="h-8 w-8 pb-1 fill-current"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
              >
                <path d={openPath} />
              </svg>
            </div>
          </div>
          <div className="py-3"></div>
          <pre className="w-full">
            <code className="language-html">{code}</code>
          </pre>
        </div>
      </div>
    );
  }
}

export default NavWithIcon;
