import React from "react";
import Prism from "prismjs";
require("../css/prism.css");

const code = `
<div className="flex justify-center w-full bg-warm-gray-200 py-6">
    <form
        className="bg-warm-gray-300 w-4/5 lg:w-2/3 py-6 px-8 lg:px-24"
        action=""
    >
        <div className="lg:flex mb-4 -mx-2">
        <div className="lg:w-1/3">
            <label className="block py-2" for="first_name">
            First Name
            </label>
        </div>
        <div className="lg:w-2/3 bg-white py-2 px-4 rounded">
            <input
            className="w-full bg-white"
            id="first_name"
            name="first_name"
            type="text"
            value=""
            />
        </div>
        </div>
        <div className="lg:flex -mx-2">
        <div className="lg:w-1/3">
            <label className="block py-2" for="first_name">
            Last Name
            </label>
        </div>
        <div className="lg:w-2/3 bg-white py-2 px-4 rounded">
            <input
            className="w-full bg-white"
            id="last_name"
            name="last_name"
            type="text"
            value=""
            />
        </div>
        </div>
    </form>
</div>
`.trim();

class AlignedTextInput extends React.Component {
  componentDidMount() {
    // You can call the Prism.js API here
    // Use setTimeout to push onto callback queue so it runs after the DOM is updated
    setTimeout(() => Prism.highlightAll(), 0);
  }
  render() {
    return (
      <div className="flex w-full justify-center">
        <div className="w-5/6">
          <h2 className="w-full text-2xl text-center pt-4 pb-6">
            Aligned Text Input
          </h2>
          <div className="flex justify-center w-full bg-warm-gray-200 py-6">
            <form
              className="bg-warm-gray-300 w-4/5 lg:w-2/3 py-6 px-8 lg:px-24"
              action=""
            >
              <div className="lg:flex mb-4 -mx-2">
                <div className="lg:w-1/3">
                  <label className="block py-2" for="first_name">
                    First Name
                  </label>
                </div>
                <div className="lg:w-2/3 bg-white py-2 px-4 rounded">
                  <input
                    className="w-full bg-white"
                    id="first_name"
                    name="first_name"
                    type="text"
                    value=""
                  />
                </div>
              </div>
              <div className="lg:flex -mx-2">
                <div className="lg:w-1/3">
                  <label className="block py-2" for="first_name">
                    Last Name
                  </label>
                </div>
                <div className="lg:w-2/3 bg-white py-2 px-4 rounded">
                  <input
                    className="w-full bg-white"
                    id="last_name"
                    name="last_name"
                    type="text"
                    value=""
                  />
                </div>
              </div>
            </form>
          </div>
          <div className="py-3"></div>
          <pre className="w-full">
            <code className="language-html">{code}</code>
          </pre>
        </div>
      </div>
    );
  }
}

export default AlignedTextInput;
