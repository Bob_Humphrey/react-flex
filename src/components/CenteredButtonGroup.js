import React from "react";
import Prism from "prismjs";
require("../css/prism.css");

const code = `
<div className="flex flex-col lg:flex-row items-center lg:justify-center w-full bg-warm-gray-200 py-10">
    <a
        className="bg-warm-gray-400 hover:bg-warm-gray-800 text-warm-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48 "
        href="/"
    >
        Send
    </a>
    <a
        className="bg-warm-gray-400 hover:bg-warm-gray-800 text-warm-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48 "
        href="/"
    >
        Cancel
    </a>
</div>
`.trim();

class CenteredButtonGroup extends React.Component {
  componentDidMount() {
    // You can call the Prism.js API here
    // Use setTimeout to push onto callback queue so it runs after the DOM is updated
    setTimeout(() => Prism.highlightAll(), 0);
  }
  render() {
    return (
      <div className="flex w-full justify-center">
        <div className="w-5/6">
          <h2 className="w-full text-2xl text-center pt-4 pb-6">
            Centered Button Group
          </h2>
          <div className="flex flex-col lg:flex-row items-center lg:justify-center w-full bg-warm-gray-200 py-10">
            <a
              className="bg-warm-gray-400 hover:bg-warm-gray-800 text-warm-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48 "
              href="/"
            >
              Send
            </a>
            <a
              className="bg-warm-gray-400 hover:bg-warm-gray-800 text-warm-gray-700 hover:text-white text-center rounded py-2 mx-2 mb-2 lg:mb-0 w-48 "
              href="/"
            >
              Cancel
            </a>
          </div>
          <div className="py-3"></div>
          <pre className="w-full">
            <code className="language-html">{code}</code>
          </pre>
        </div>
      </div>
    );
  }
}

export default CenteredButtonGroup;
