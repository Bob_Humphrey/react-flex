import React from "react";

const Heading = () => {
  return (
    <div className="flex w-full justify-center py-6">
      <div className="lg:flex w-2/3 lg:-mx-2">
        <div className="w-full lg:w-1/3 lg:mx-2 mb-2 lg:mb-0">
          <h3 className="font-bold mb-2">Main Axis</h3>
          <ul className="text-warm-gray-700 mb-2">
            <li className="">justify-start</li>
            <li className="">justify-center</li>
            <li className="">justify-end</li>
            <li className="">justify-between</li>
            <li className="">justify-around</li>
          </ul>
          <h3 className="font-bold mb-2">Cross Axis</h3>
          <ul className="text-warm-gray-700 mb-2">
            <li className="">self-start</li>
            <li className="">self-center</li>
            <li className="">self-end</li>
            <li className="">self-auto</li>
            <li className="">self-stretch</li>
          </ul>
        </div>
        <div className="w-full lg:w-1/3 lg:mx-2 mb-2 lg:mb-0">
          <h3 className="font-bold mb-2">Multi Line</h3>
          <ul className="text-warm-gray-700 mb-2">
            <li className="">flex-wrap</li>
            <li className="">flex-no-wrap</li>
          </ul>
          <h3 className="font-bold mb-2">Align Rows Vertically</h3>
          <ul className="text-warm-gray-700 mb-2">
            <li className="">content-start</li>
            <li className="">content-center</li>
            <li className="">content-end</li>
            <li className="">content-between</li>
            <li className="">content-around</li>
          </ul>
        </div>
        <div className="w-full lg:w-1/3 lg:mx-2 mb-2 lg:mb-0">
          <h3 className="font-bold mb-2">Direction</h3>
          <ul className="text-warm-gray-700 mb-2">
            <li className="">flex-row</li>
            <li className="">flex-col</li>
          </ul>
          <h3 className="font-bold mb-2">Links</h3>
          <ul className="text-warm-gray-700 mb-2">
            <li className="w-full">
              <a
                className="hover:text-teal-700"
                href="https://tailwindcss.com/"
                target="_blank"
                rel="noopener noreferrer"
              >
                Tailwind CSS
              </a>
            </li>
            <li className="w-full">
              <a
                className="hover:text-teal-700"
                href="https://nerdcave.com/tailwind-cheat-sheet"
                target="_blank"
                rel="noopener noreferrer"
              >
                Tailwind Cheat Sheet
              </a>
            </li>
            <li className="w-full">
              <a
                className="hover:text-teal-700"
                href="https://yoksel.github.io/flex-cheatsheet/"
                target="_blank"
                rel="noopener noreferrer"
              >
                Yoksel Flexbox Cheat Sheet
              </a>
            </li>
            <li className="w-full">
              <a
                className="hover:text-teal-700"
                href="https://philipwalton.github.io/solved-by-flexbox/"
                target="_blank"
                rel="noopener noreferrer"
              >
                Solved by Flexbox
              </a>
            </li>
            <li className="w-full">
              <a
                className="hover:text-teal-700"
                href="https://csslayout.io/"
                target="_blank"
                rel="noopener noreferrer"
              >
                CSS Layout
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Heading;
