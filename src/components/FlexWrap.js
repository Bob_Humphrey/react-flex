import React from "react";
import Prism from "prismjs";
require("../css/prism.css");

const code = `
<div className="w-full bg-warm-gray-200 p-8">
    <div className="flex flex-wrap -mx-1">
        <div className="w-1/2 lg:w-1/4 px-1 mb-2">
            <div className="bg-warm-gray-400 h-32"></div>
        </div>
        <div className="w-1/2 lg:w-1/4 px-1 mb-2">
            <div className="bg-warm-gray-400 h-32"></div>
        </div>
        <div className="w-1/2 lg:w-1/4 px-1 mb-2">
            <div className="bg-warm-gray-400 h-32"></div>
        </div>
        <div className="w-1/2 lg:w-1/4 px-1 mb-2">
            <div className="bg-warm-gray-400 h-32"></div>
        </div>
        <div className="w-1/2 lg:w-1/4 px-1 mb-2">
            <div className="bg-warm-gray-400 h-32"></div>
        </div>
        <div className="w-1/2 lg:w-1/4 px-1 mb-2">
            <div className="bg-warm-gray-400 h-32"></div>
        </div>
        <div className="w-1/2 lg:w-1/4 px-1 mb-2">
            <div className="bg-warm-gray-400 h-32"></div>
        </div>
        <div className="w-1/2 lg:w-1/4 px-1 mb-2">
            <div className="bg-warm-gray-400 h-32"></div>
        </div>
    </div>
</div>
`.trim();

class FlexWrap extends React.Component {
  componentDidMount() {
    // You can call the Prism.js API here
    // Use setTimeout to push onto callback queue so it runs after the DOM is updated
    setTimeout(() => Prism.highlightAll(), 0);
  }
  render() {
    return (
      <div className="flex w-full justify-center">
        <div className="w-5/6">
          <h2 className="w-full text-2xl text-center pt-4 pb-6">Flex Wrap</h2>
          <div className="w-full bg-warm-gray-200 p-8">
            <div className="flex flex-wrap -mx-1">
              <div className="w-1/2 lg:w-1/4 px-1 mb-2">
                <div className="bg-warm-gray-400 h-32"></div>
              </div>
              <div className="w-1/2 lg:w-1/4 px-1 mb-2">
                <div className="bg-warm-gray-400 h-32"></div>
              </div>
              <div className="w-1/2 lg:w-1/4 px-1 mb-2">
                <div className="bg-warm-gray-400 h-32"></div>
              </div>
              <div className="w-1/2 lg:w-1/4 px-1 mb-2">
                <div className="bg-warm-gray-400 h-32"></div>
              </div>
              <div className="w-1/2 lg:w-1/4 px-1 mb-2">
                <div className="bg-warm-gray-400 h-32"></div>
              </div>
              <div className="w-1/2 lg:w-1/4 px-1 mb-2">
                <div className="bg-warm-gray-400 h-32"></div>
              </div>
              <div className="w-1/2 lg:w-1/4 px-1 mb-2">
                <div className="bg-warm-gray-400 h-32"></div>
              </div>
              <div className="w-1/2 lg:w-1/4 px-1 mb-2">
                <div className="bg-warm-gray-400 h-32"></div>
              </div>
            </div>
          </div>
          <div className="py-3"></div>
          <pre className="w-full">
            <code className="language-html">{code}</code>
          </pre>
        </div>
      </div>
    );
  }
}

export default FlexWrap;
