import React from "react";
import Prism from "prismjs";
require("../css/prism.css");

const code = `
<div className=" flex flex-col lg:flex-row w-full justify-center text-warm-gray-700 border-t border-l border-r border-warm-gray-200 pt-4 pb-4 px-6">
    <div className="w-1/4">Data</div>
    <div className="w-1/4">Data</div>
    <div className="w-1/4">Data</div>
    <div className="w-1/4">Data</div>
</div>
`.trim();

class TableRow extends React.Component {
  componentDidMount() {
    // You can call the Prism.js API here
    // Use setTimeout to push onto callback queue so it runs after the DOM is updated
    setTimeout(() => Prism.highlightAll(), 0);
  }
  render() {
    return (
      <div className="flex w-full justify-center">
        <div className="w-5/6">
          <h2 className="w-full text-2xl text-center pt-4 pb-6">Table Row</h2>
          <div className=" flex flex-col lg:flex-row w-full justify-center text-warm-gray-700 border-t border-l border-r border-warm-gray-200 pt-4 pb-4 px-6">
            <div className="w-1/4">Data</div>
            <div className="w-1/4">Data</div>
            <div className="w-1/4">Data</div>
            <div className="w-1/4">Data</div>
          </div>
          <div className="py-3"></div>
          <pre className="w-full">
            <code className="language-html">{code}</code>
          </pre>
        </div>
      </div>
    );
  }
}

export default TableRow;
