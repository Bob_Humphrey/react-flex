import React from "react";
import Prism from "prismjs";
import logo from "./../images/bh-logo-grey.gif";
require("../css/prism.css");

const code = `
import logo from "./../images/bh-logo-grey.gif";

<footer className="flex w-full justify-center bg-warm-gray-200 py-16">
    <div className="w-16">
        <a href="https://bob-humphrey.com">
        <img src={logo} alt="Logo" />
        </a>
    </div>
</footer>
`.trim();

class SimpleFooter extends React.Component {
  componentDidMount() {
    // You can call the Prism.js API here
    // Use setTimeout to push onto callback queue so it runs after the DOM is updated
    setTimeout(() => Prism.highlightAll(), 0);
  }
  render() {
    return (
      <div className="flex w-full justify-center">
        <div className="w-5/6">
          <h2 className="w-full text-2xl text-center pt-4 pb-6">Simple Footer</h2>
          <footer className="flex w-full justify-center bg-warm-gray-200 py-16">
            <div className="w-16">
              <a href="https://bob-humphrey.com">
                <img src={logo} alt="Logo" />
              </a>
            </div>
          </footer>
          <div className="py-3"></div>
          <pre className="w-full">
            <code className="language-html">{code}</code>
          </pre>
        </div>
      </div>
    );
  }
}

export default SimpleFooter;
