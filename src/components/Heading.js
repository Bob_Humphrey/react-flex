import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import Notes from "./Notes";

const Heading = () => {
  const [showNotes, setShowNotes] = useState(false);
  const notesDisplayClass = showNotes ? "flex" : "hidden";
  const openPath =
    "M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zM9 11v4h2V9H9v2zm0-6v2h2V5H9z";
  const closePath =
    "M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zM11.4 10l2.83-2.83-1.41-1.41L10 8.59 7.17 5.76 5.76 7.17 8.59 10l-2.83 2.83 1.41 1.41L10 11.41l2.83 2.83 1.41-1.41L11.41 10z";
  const notesIconPath = showNotes ? closePath : openPath;
  return (
    <div className="flex w-full justify-center">
      <div className="w-5/6 px-2">
        <div className="flex flex-col lg:flex-row w-full items-center lg:justify-between">
          <NavLink to="/">
            <div className="flex justify-start pt-5 text-teal-700">
              <div className="text-3xl text-center leading-tight">
                Tailwind CSS Flexbox Patterns
              </div>
            </div>
          </NavLink>
          <div
            className="flex justify-end self-center lg:self-end text-teal-800 cursor-pointer mt-2"
            onClick={() => setShowNotes(!showNotes)}
          >
            <svg
              className="h-8 w-8 pb-1 fill-current"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
            >
              <path d={notesIconPath} />
            </svg>
          </div>
        </div>
        <div className={notesDisplayClass + ""}>
          <Notes />
        </div>
      </div>
    </div>
  );
};

export default Heading;
