import React from "react";
import Prism from "prismjs";
require("../css/prism.css");

const code = `
<div class="flex w-full lg:w-1/3 px-2 mb-4 items-stretch">
  <div class="bg-warm-gray-200 rounded-lg">
    <div class="rounded-t-lg bg-warm-gray-400 h-32"> </div>
    <div class="py-4 px-8">
      <h3 class="font-bold text-warm-gray-700 text-2xl pb-2">
        Title
      </h3>
      <div class="pb-6 text-justify">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Curabitur eget elit eu urna gravida semper quis eget quam.
      </div>
      <div class="flex justify-center">
        <div class="bg-warm-gray-400 hover:bg-warm-gray-800 text-warm-gray-700 hover:text-white rounded px-8 py-1 mr-2 mb-2">
          <a class="" href="/">
            Visit Site
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
`.trim();

class Card extends React.Component {
  componentDidMount() {
    // You can call the Prism.js API here
    // Use setTimeout to push onto callback queue so it runs after the DOM is updated
    setTimeout(() => Prism.highlightAll(), 0);
  }
  render() {
    return (
      <div className="flex w-full justify-center">
        <div className="w-5/6">
          <h2 className="w-full text-2xl text-center pt-4 pb-6">Card</h2>
          <div className="lg:flex justify-center">
            <div class="flex w-full lg:w-1/3 px-2 mb-4 items-stretch">
              <div class="bg-warm-gray-200 rounded-lg">
                <div class="rounded-t-lg bg-warm-gray-400 h-32"> </div>
                <div class="py-4 px-8">
                  <h3 class="font-bold text-warm-gray-700 text-2xl pb-2">
                    Title
                  </h3>
                  <div class="pb-6 text-justify">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Curabitur eget elit eu urna gravida semper quis eget quam.
                  </div>
                  <div class="flex justify-center">
                    <div class="bg-warm-gray-400 hover:bg-warm-gray-800 text-warm-gray-700 hover:text-white rounded px-8 py-1 mr-2 mb-2">
                      <a class="" href="/">
                        Visit Site
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="py-3"></div>
          <pre className="w-full">
            <code className="language-html">{code}</code>
          </pre>
        </div>
      </div>
    );
  }
}

export default Card;
